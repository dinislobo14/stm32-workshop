# STM32 Workshop

This repository has all the content of the STM32 workshop. All slides, code, documentation, and links to the support videos.

The repo is divided into three parts:

- Slides
- Code
- Documentation

### Slides

Here you can find all the presentations given through the in-person sessions.

### Code

In this directory you will find all the examples that appear in the slides.

### Documentation

All documentation used to create this course, you may want to take a look at it in some cases of doubt. Some of those documents are not updated on purpose, due to st new documentation changes like the removal of some information.

## Content

The topics taught in the course are:

- Create a project
- HAL API
- GPIO
- Interrupts and NVIC
- Debug on-chip
- UART
- Clocks and Clock Tree
- Timers
- ADC
- DAC

## Set up of the machine and Installation of the tools

All the steps required to accomplish the installation of the tools are in the session 0 slides.

However, the installation presented only works on Windows OS, if you want to work in other OS (like macOS or Linux) you can install a Virtual Machine with Windows OS, withal that those Virtual Machine must be capable of interact with Serial Port (RS232) and USB (version 2.0 at least). If a Virtual Machine isn't a solution for you, you can install a different set of tools, our recommendation, in that case, is to use the STM32CubeIDE. You can find an installation tutorial [here](https://www.st.com/content/ccc/resource/technical/document/user_manual/group1/31/8b/03/27/25/c5/4d/ae/DM00603964/files/DM00603964.pdf/jcr:content/translations/en.DM00603964.pdf)

Keep in mind that the projects are made to ARM Keil, so if you install another toolset you should make the needed changes to make the code work.

### Requirements

In order to get the most profit over the course you should have:

- Computer with Windows OS (or other like told previously)
- STM32 development board (STM32 Nucleo-F767ZI preferably)
- Basic knowledge about embedded systems
- Basic Knowledge of embedded C programming language

## Usage and Purpose

The purpose of all this content is to help the students of MIEEEIC to learn the basics of STM32 development. It is intended that at the end of the planned sessions the students should have enough curiosity to continue learning.

All the code given may be used without asking for authorization, nonetheless, using it for evaluation purposes is not recommended and will not contribute to the main purpose of this curse: "helping people to learn".

## Authors

This work is being developed by a team over the past few years now, all having some kind of contribution:

- Álvaro Castro Leite @alvarocleite
- Filipa Araújo @filipaaraujo
- Daniel Gomes @danielgomescames
- Tiago Aston @OBCrispy
- João Azevedo @J-Azevedo
- Vitor Ribeiro @vitorribeiro99
- Sofia Paiva @sofiapaiva
- João Borges @jmmb13 
- Hugo Carvalho @Hugo_dCarvalho
- José Mendes @josepr.mendes 

### Videos Contributions

- Álvaro Castro Leite @alvarocleite
- Vitor Ribeiro @vitorribeiro99
- Sofia Paiva @sofiapaiva
- João Borges @jmmb13 

## Licensing

This project is released under the BSD-3-Clause License. Please read the license.txt file. 
